# Probleemomschrijving
..geef hier een beknopte omschrijving van de bevinding..

# Impact
..geef aan wat er niet goed gaat als de bevinding niet opgepakt wordt EN op welke onderdelen van de keten dit impact heeft..

..als de bevinding blokkerend is voor het gebruik van de standaard in productie (release A), geef dan ook aan waarom dit blokkerend is - anders wordt de bevinding voor release B opgepakt..

# Betrokken partijen
..geef aan wie ermee gebaat en betrokken is bij de oplossing van dit issue..

# Oplossingsrichting(en)
..geef een suggestie wat er in de standaard moet veranderen om
de bevinding op te lossen (mogelijk aangevuld door KOOP/PR34)..

# Gerealiseerde oplossing
..in te vullen door KOOP/PR34..
