# Standaard Officiële Publicaties 

Dit is de repository van de STOP-standaard.
In de master branch van de repository staan geen bestanden. 

- Zie het [documentatie-overzicht](https://koop.gitlab.io/STOP/standaard/index.html) voor een overzicht van de beschikbare versies van de standaard. 

- De schemata, voorbeelden en waardelijsten kunnen in deze repository worden teruggevonden onder de *tag* met dezelfde naam als het versienummer van de standaard. Deze kunnen worden verkregen door:
  - in de ["Web-interface van Gitlab"](https://gitlab.com/koop/STOP/standaard/) eerst het versienummer te kiezen (in plaats van *master*) en daarna de repository te downloaden;
  - met een eigen Git-client deze repository te klonen, daarna de tag op te zoeken en op basis van de tag een [Git kloon](https://git-scm.com/docs/git-clone) te maken of een [Git switch](https://git-scm.com/docs/git-switch) uit te voeren.
